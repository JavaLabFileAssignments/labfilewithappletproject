package answer19;

import java.awt.*;
import java.applet.*;
import java.awt.event.*;
public class StudentReport extends Applet implements ActionListener
    {
        Label title, registerNo, name, java, springBoot, database;
        TextField regNo, sName,txtJava, txtSpring, txtDatabase;
        Button report;
        int total;
        float average;

        public void init()
        {
            setLayout(null);
            title =new Label("Enter Student’s Details:");
            registerNo =new Label("Register No:");
            name =new Label("Name:");
            java =new Label("Java:");
            springBoot =new Label("Spring boot:");
            database =new Label("Database:");

            regNo =new TextField(12);
            sName =new TextField(40);
            txtJava=new TextField(4);
            txtSpring =new TextField(4);
            txtDatabase =new TextField(4);

            report =new Button("View Student Result");
            title.setBounds(100,0,200,20);
            registerNo.setBounds(0,50,100,20);
            regNo.setBounds(120,50,100,20);
            name.setBounds(0,75,100,20);
            sName.setBounds(120,75,250,20);
            java.setBounds(0,100,100,20);
            txtJava.setBounds(120,100,40,20);
            springBoot.setBounds(0,125,100,20);
            txtSpring.setBounds(120,125,40,20);
            database.setBounds(0,150,100,20);
            txtDatabase.setBounds(120,150,40,20);

            report.setBounds(100,225,150,30);
            add(title);
            add(registerNo);
            add(regNo);
            add(name);
            add(sName);
            add(java);
            add(txtJava);
            add(springBoot);
            add(txtSpring);
            add(database);
            add(txtDatabase);
            add(report);
            report.addActionListener(this);
        }
        public void actionPerformed(ActionEvent ae)
        {
            try
            {
                int java=Integer.parseInt(txtJava.getText());
                int se=Integer.parseInt(txtSpring.getText());
                int ca=Integer.parseInt(txtDatabase.getText());

                total=(java+se+ca);
                average =total/3;
            }
            catch(NumberFormatException e)
            {
            }
            repaint();
        }
        public void paint(Graphics g)
        {
            g.drawString("Student Report",100,275);
            g.drawString("Register No.: " + regNo.getText(),0,300);
            g.drawString("Name : " + sName.getText(),0,325);
            g.drawString("Java:  " + txtJava.getText(),0,350);
            g.drawString("Spring boot : " + txtSpring.getText(),0,375);
            g.drawString("Database : " + txtDatabase.getText(),0,400);
            g.drawString("Total: " + total,0,475);
            g.drawString("Average: " + average,0,500);
        }
    }

