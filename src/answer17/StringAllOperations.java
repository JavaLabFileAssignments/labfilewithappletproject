package answer17;

import java.util.*;
public class StringAllOperations
{
        static Scanner scanner = new Scanner(System.in);
        public static void main(String[] args)
        {
            String first;
            String second;
            System.out.println("String Operation");
            System.out.print("Enter the first string type: ");
            first=scanner.nextLine();
            System.out.print("Enter the second string type: ");
            second=scanner.nextLine();
            System.out.println("If string type is empty,it will show true,otherwise false. Checking = " + first.isEmpty());
            System.out.println("The length of the first string is :" + first.length());
            System.out.println("The length of the second string is :" + second.length());
            System.out.println("The concatenation of first and second string types = " + first.concat(" " + second));
            System.out.println("Enter an index which you want to show letter from " + first);
            int index = scanner.nextInt();
            System.out.println("The first character of " + first + " is: " + first.charAt(index));
            System.out.println("The uppercase of " + first + " = " + first.toUpperCase());
            System.out.println("The lowercase of " + first + " = " + first.toLowerCase());
            System.out.print("Enter the occurance of a character in " + first + " = ");
            String string = scanner.next();
            char c = string.charAt(0);
            System.out.println("The "+c+" occurs at position " + first.indexOf(c) + " in " + first);
            System.out.println("The substring of " + first + " starting from index 1 and ending at 5 is: " + first.substring(1,6));
            System.out.println("Replacing 'a' with 'o' in " + first + " = " + first.replace('i','a'));
            boolean check = first.equals(second);
            if(!check)
                System.out.println(first + " and " + second + " are not same.");
            else
                System.out.println(first + " and " + second + " are same.");
        }
}
