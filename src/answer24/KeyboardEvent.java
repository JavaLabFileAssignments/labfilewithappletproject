package answer24;

import java.awt.*;
import java.awt.event.*;
    public class KeyboardEvent extends Frame implements KeyListener
    {
        Label label;
        TextArea area;
        KeyboardEvent(){

            label = new Label();
            label.setBounds(20,50,100,20);
            area=new TextArea();
            area.setBounds(20,80,300, 300);
            add(label);
            add(area);
            area.addKeyListener(this);

            setSize(400,400);
            setLayout(null);
            setVisible(true);
        }
        public void keyPressed(KeyEvent keyEvent) {
            label.setText("Key Pressed");
        }
        public void keyReleased(KeyEvent keyEvent) {
            label.setText("Key Released");
        }
        public void keyTyped(KeyEvent keyEvent) {
            label.setText("Key Typed");
        }

        public static void main(String[] args) {
            new KeyboardEvent();
        }
    }
//        static JFrame frame;
//        static JTextField output;
//        static JTextField input;
//        public static void main(String args[])
//        {
//            frame=new JFrame("Keyboard Event");
//            frame.setBackground(Color.gray);
//            frame.setSize(400,400);
//            frame.setLayout(null);
//            output=new JTextField();
//            output.setBounds(0,0,400,40);
//            frame.add(output);
//            input=new JTextField();
//            input.setBounds(0,400,400,40);
//            frame.add(input);
//            JButton exit=new JButton("Exit");
//            exit.setBounds(150,150,60,30);
//            frame.add(exit);
//            KeyboardEvent keyboardEvent=new KeyboardEvent();
//            input.addKeyListener(keyboardEvent);
//            exit.addActionListener(keyboardEvent);
//            frame.setVisible(true);
//        }
//        @Override
//        public void actionPerformed(ActionEvent actionEvent)
//        {
//            frame.dispose();
//        }
//        @Override
//        public void keyReleased(KeyEvent keyEvent)
//        {
//            output.setText("");
//            output.setText("Key Released : " + keyEvent.getKeyCode());
//            if(Character.isLetter(keyEvent.getKeyChar()))
//                keyTyped(keyEvent);
//            if(Character.isDigit(keyEvent.getKeyChar()))
//                keyTyped(keyEvent);
//        }
//        @Override
//        public void keyPressed(KeyEvent keyEvent)
//        {
//            output.setText("");
//            output.setText("Key Pressed : " + keyEvent.getKeyCode());
//            if(Character.isLetter(keyEvent.getKeyChar()))
//                keyTyped(keyEvent);
//            if(Character.isDigit(keyEvent.getKeyChar()))
//                keyTyped(keyEvent);
//        }
//        @Override
//        public void keyTyped(KeyEvent keyEvent)
//        {
//            output.setText("");
//            output.setText("Key Typed : " + keyEvent.getKeyChar());
//        }


