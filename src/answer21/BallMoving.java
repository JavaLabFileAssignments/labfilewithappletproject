package answer21;

import java.awt.*;
import java.applet.*;
public class BallMoving extends Applet implements Runnable
    {
        int x,y,r;
        int dx,dy;

        @Override
        public void init() {
            setBackground(Color.cyan);
            x = 150;
            y = 50;
            r = 20;
            dx = 11;
            dy = 7;
        }

        Thread thread;
        boolean stopFlag;

        public void start()
        {
            thread = new Thread(this);
            stopFlag=false;
            thread.start();
        }

        public void paint(Graphics graphics)
        {
            graphics.setColor(Color.green);
            graphics.fillOval(x-r, y-r, r*2, r*2);
        }

        public void run()
        {
            while(true)
            {
                if(stopFlag)
                    break;
                if ((x - r + dx < 0) || (x + r + dx > bounds().width)) dx = -dx;
                if ((y - r + dy < 0) || (y + r + dy > bounds().height)) dy = -dy;
                // Move the circle.
                x += dx;  y += dy;

                try
                {
                    Thread.sleep(100);
                }
                catch(Exception e)
                {
                    System.out.println(e);
                };
                repaint();
            }
        }

        public void stop()
        {
            stopFlag = true;
            thread = null;
        }
}

