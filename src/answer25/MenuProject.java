package answer25;

import java.awt.*;
public class MenuProject {
    MenuProject(){
        Frame frame = new Frame("Menu and MenuBar");
        MenuBar menuBar = new MenuBar();
        Menu menu = new Menu("Menu");
        Menu submenu = new Menu("Sub Menu");
        MenuItem home = new MenuItem("Home");
        MenuItem guest = new MenuItem("Guest");
        MenuItem education = new MenuItem("Education");
        MenuItem contact = new MenuItem("Contact");
        MenuItem skills = new MenuItem("Skills");
        menu.add(home);
        menu.add(guest);
        menu.add(education);
        submenu.add(contact);
        submenu.add(skills);
        menu.add(submenu);
        menuBar.add(menu);
        frame.setMenuBar(menuBar);
        frame.setSize(500,500);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    public static void main(String args[])
    {
        new MenuProject();
    }
}
